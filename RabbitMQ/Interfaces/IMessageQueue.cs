﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Interfaces
{
    public interface IMessageQueue
    {
        IModel Channel { get;  set; }

        void DeclareExchange(string exchangeName, string exchangeType);

        void BindQueue(string exchangeName, string routingKey, string queueName);

        void Dispose();


    }
}
