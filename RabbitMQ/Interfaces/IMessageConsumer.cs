﻿using RabbitMQ.Client.Events;
using RabbitMQ.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Interfaces
{
    public interface IMessageConsumer
    {
        void Connect();
        public event EventHandler<BasicDeliverEventArgs> Received;
        void SetAcknoledge(ulong deliveryTag, bool proceed);
    }
}
