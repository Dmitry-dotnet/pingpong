﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Interfaces
{
    public interface IMessageProducerScope
    {
        IMessageProducer MessageProducer { get; }
        void Dispose();

    }
}
