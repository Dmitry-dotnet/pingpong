﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Interfaces
{
    public interface IMessageConsumerScope
    {
        IMessageConsumer MessageConsumer { get; }
        void Dispose();
    }
}
