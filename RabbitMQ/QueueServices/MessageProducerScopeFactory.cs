﻿using RabbitMQ.Client;
using RabbitMQ.Interfaces;
using RabbitMQ.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.QueueServices
{
    public class MessageProducerScopeFactory : IMessageProducerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public IMessageProducerScope Open(MessageScopeSettings messageScope)
        {
            return new MessageProducerScope(_connectionFactory, messageScope);
        }
    }
}
