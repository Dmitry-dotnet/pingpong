﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Interfaces;
using RabbitMQ.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RabbitMQ.Services
{
    public class PingPongService : IDisposable
    {
        protected readonly IMessageConsumerScope _messageConsumerScope;
        protected readonly IMessageProducerScope _messageProducerScope;

        public PingPongService(
            IMessageConsumerScopeFactory consumerScopeFactory,
            IMessageProducerScopeFactory producerScopeFactory,
            MessageScopeSettings consumerSettings,
            MessageScopeSettings producerSettings)
        {
            _messageConsumerScope = consumerScopeFactory.Connect(consumerSettings);
            _messageConsumerScope.MessageConsumer.Received += MessageReceived;

            _messageProducerScope = producerScopeFactory.Open(producerSettings);
        }
        public void Run(string serviceName)
        {
            Console.WriteLine($"{serviceName} Service started");
        }

        protected void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            bool proceed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body.ToArray());
                Console.WriteLine($"{value} received at {DateTime.Now}");
                Thread.Sleep(2500);
                proceed = true;

            }
            catch (Exception)
            {
                proceed = false;
                throw;
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetAcknoledge(0, proceed);
                SendMessage();

            }
        }

        public virtual void SendMessage()
        {
            _messageProducerScope.MessageProducer.Send("Message");
            Console.WriteLine($"Message sent at {DateTime.Now}");
        }

        public void Dispose()
        {
            _messageProducerScope.Dispose();
            _messageConsumerScope.Dispose();
        }
    }
}
