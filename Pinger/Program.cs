﻿using Pinger.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Interfaces;
using RabbitMQ.Models;
using RabbitMQ.QueueServices;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

namespace Pinger
{
    public class Program
    {
        static void Main(string[] args)
        {

            MessageScopeSettings consumerSettings = new MessageScopeSettings()
            {
                ExchangeName = "PingExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            };
            MessageScopeSettings producerSettings = new MessageScopeSettings()
            {
                ExchangeName = "PongExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pong_queue",
                RoutingKey = "pong"
            };
        
            PingerService service = new PingerService(new MessageConsumerScopeFactory(new ConnectionFactory()),
                                                      new MessageProducerScopeFactory(new ConnectionFactory()),
                                                      consumerSettings, producerSettings);
            service.SendMessage();
            Console.Read();
        }
    }
}
