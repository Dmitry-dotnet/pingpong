﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Interfaces;
using RabbitMQ.Models;
using RabbitMQ.Services;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Channels;

namespace Pinger.Services
{
    public class PingerService : PingPongService
    {

        public PingerService(IMessageConsumerScopeFactory consumerScopeFactory,
                             IMessageProducerScopeFactory producerScopeFactory,
                             MessageScopeSettings consumerSettings,
                             MessageScopeSettings producerSettings) : base
                                (consumerScopeFactory, 
                                 producerScopeFactory, 
                                 consumerSettings, 
                                 producerSettings)
        {
        
        }

        public override void SendMessage()
        {
            _messageProducerScope.MessageProducer.Send("Ping");
            Console.WriteLine($"Ping sent at {DateTime.Now}");
        }

    }

}
