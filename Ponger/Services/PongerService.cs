﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Interfaces;
using RabbitMQ.Models;
using RabbitMQ.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Ponger.Services
{
    public class PongerService : PingPongService
    {
        public PongerService(IMessageConsumerScopeFactory consumerScopeFactory,
                            IMessageProducerScopeFactory producerScopeFactory,
                            MessageScopeSettings consumerSettings,
                            MessageScopeSettings producerSettings) 
                            : base(consumerScopeFactory,
                                producerScopeFactory,
                                consumerSettings,
                                producerSettings)
        {

        }

        public override void SendMessage()
        {
            _messageProducerScope.MessageProducer.Send("Pong");
            Console.WriteLine($"Pong sent at {DateTime.Now}");
        }
    }
}
