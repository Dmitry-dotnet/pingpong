﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using Ponger.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Models;
using RabbitMQ.QueueServices;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            MessageScopeSettings consumerSettings = new MessageScopeSettings()
            {
                ExchangeName = "PongExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pong_queue",
                RoutingKey = "pong"
            };
            MessageScopeSettings producerSettings = new MessageScopeSettings()
            {
                ExchangeName = "PingExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            };
            PongerService service = new PongerService(new MessageConsumerScopeFactory(new ConnectionFactory()),
                                                      new MessageProducerScopeFactory(new ConnectionFactory()),
                                                      consumerSettings, producerSettings);
            Console.Read();

        }
      
    }
}

